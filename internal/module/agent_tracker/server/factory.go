package server

import (
	"context"
	"time"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/agent_tracker"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/agent_tracker/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/modshared"
	otelmetric "go.opentelemetry.io/otel/metric"
)

const (
	connectedAgentsCounterName    = "connected_agents_count"
	connectedAgentPodsCounterName = "connected_agent_pods_count"
)

type Factory struct {
	AgentQuerier agent_tracker.Querier
}

func (f *Factory) New(config *modserver.Config) (modserver.Module, error) {
	_, err := config.Meter.Int64ObservableGauge(
		connectedAgentsCounterName,
		otelmetric.WithDescription("The number of unique connected agent IDs"),
		otelmetric.WithInt64Callback(f.countConnectedAgents),
	)
	if err != nil {
		return nil, err
	}

	_, err = config.Meter.Int64ObservableGauge(
		connectedAgentPodsCounterName,
		otelmetric.WithDescription("The number of unique connected agent Pods"),
		otelmetric.WithInt64Callback(f.countConnectedAgentPods),
	)
	if err != nil {
		return nil, err
	}

	rpc.RegisterAgentTrackerServer(config.APIServer, &server{
		agentQuerier: f.AgentQuerier,
	})

	return &modserver.NopModule{
		ModuleName: agent_tracker.ModuleName,
	}, nil
}

func (f *Factory) Name() string {
	return agent_tracker.ModuleName
}

func (f *Factory) StartStopPhase() modshared.ModuleStartStopPhase {
	return modshared.ModuleStartBeforeServers
}

func (f *Factory) countConnectedAgents(ctx context.Context, observer otelmetric.Int64Observer) error {
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()
	size, err := f.AgentQuerier.GetConnectedAgentsCount(ctx)
	if err != nil {
		return err
	}
	observer.Observe(size)
	return nil
}

func (f *Factory) countConnectedAgentPods(ctx context.Context, observer otelmetric.Int64Observer) error {
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()
	size, err := f.AgentQuerier.GetConnectionsCount(ctx)
	if err != nil {
		return err
	}
	observer.Observe(size)
	return nil
}
