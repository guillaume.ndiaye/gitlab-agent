package agent_tracker //nolint:stylecheck

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/redistool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/testing/matcher"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/testing/mock_redis"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/testing/mock_tool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/pkg/entity"
	"go.uber.org/mock/gomock"
	"go.uber.org/zap/zaptest"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/testing/protocmp"
	"google.golang.org/protobuf/types/known/timestamppb"
)

var (
	_ Registerer                 = &RedisTracker{}
	_ ExpiringRegisterer         = &RedisTracker{}
	_ Querier                    = &RedisTracker{}
	_ Tracker                    = &RedisTracker{}
	_ ConnectedAgentInfoCallback = (&ConnectedAgentInfoCollector{}).Collect
)

func TestRegisterConnection_HappyPath(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	r, connectedAgents, connections, byAgentID, byProjectID, agentVersions, connectionsByAgentVersion, _, info := setupTracker(t)

	byProjectID.EXPECT().
		Set(gomock.Any(), info.ProjectId, info.ConnectionId, gomock.Any())
	byAgentID.EXPECT().
		Set(gomock.Any(), info.AgentId, info.ConnectionId, gomock.Any())
	connectedAgents.EXPECT().
		Set(gomock.Any(), ignoredConnectedAgentsKey, info.AgentId, gomock.Any())
	connections.EXPECT().
		Set(gomock.Any(), ignoredConnectedAgentsKey, info.ConnectionId, gomock.Any())
	agentVersions.EXPECT().
		SetEX(gomock.Any(), ignoredAgentVersionKey, info.AgentMeta.Version, gomock.Any(), gomock.Any())
	connectionsByAgentVersion.EXPECT().
		SetEX(gomock.Any(), info.AgentMeta.Version, info.ConnectionId, gomock.Any(), gomock.Any())

	go func() {
		assert.NoError(t, r.RegisterConnection(context.Background(), info))
		cancel()
	}()

	require.NoError(t, r.Run(ctx))
}

func TestRegisterConnection_AllCalledOnError(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	r, connectedAgents, connections, byAgentID, byProjectID, agentVersions, connectionsByAgentVersion, _, info := setupTracker(t)

	err1 := errors.New("err1")
	err2 := errors.New("err2")
	err3 := errors.New("err3")
	err4 := errors.New("err4")
	err5 := errors.New("err5")
	err6 := errors.New("err6")

	byProjectID.EXPECT().
		Set(gomock.Any(), info.ProjectId, info.ConnectionId, gomock.Any()).
		Return(err1)
	byAgentID.EXPECT().
		Set(gomock.Any(), info.AgentId, info.ConnectionId, gomock.Any()).
		Return(err2)
	connectedAgents.EXPECT().
		Set(gomock.Any(), ignoredConnectedAgentsKey, info.AgentId, gomock.Any()).
		Return(err3)
	connections.EXPECT().
		Set(gomock.Any(), ignoredConnectionsKey, info.ConnectionId, gomock.Any()).
		Return(err3)
	agentVersions.EXPECT().
		SetEX(gomock.Any(), ignoredAgentVersionKey, info.AgentMeta.Version, gomock.Any(), gomock.Any()).
		Return(err4)
	connectionsByAgentVersion.EXPECT().
		SetEX(gomock.Any(), info.AgentMeta.Version, info.ConnectionId, gomock.Any(), gomock.Any()).
		Return(err5)

	go func() {
		err := r.RegisterConnection(context.Background(), info)

		assert.True(t, containsError(err, err1, err2, err3, err4, err5, err6))
		cancel()
	}()

	require.NoError(t, r.Run(ctx))
}

func TestUnregisterConnection_HappyPath(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	r, connectedAgents, connections, byAgentID, byProjectID, agentVersions, connectionsByAgentVersion, _, info := setupTracker(t)

	gomock.InOrder(
		byProjectID.EXPECT().
			Set(gomock.Any(), info.ProjectId, info.ConnectionId, gomock.Any()),
		byProjectID.EXPECT().
			Unset(gomock.Any(), info.ProjectId, info.ConnectionId),
	)
	gomock.InOrder(
		byAgentID.EXPECT().
			Set(gomock.Any(), info.AgentId, info.ConnectionId, gomock.Any()),
		byAgentID.EXPECT().
			Unset(gomock.Any(), info.AgentId, info.ConnectionId),
	)
	gomock.InOrder(
		connectedAgents.EXPECT().
			Set(gomock.Any(), ignoredConnectedAgentsKey, info.AgentId, gomock.Any()),
		connectedAgents.EXPECT().
			Forget(ignoredConnectedAgentsKey, info.AgentId),
	)
	gomock.InOrder(
		connections.EXPECT().
			Set(gomock.Any(), ignoredConnectionsKey, info.ConnectionId, gomock.Any()),
		connections.EXPECT().
			Forget(ignoredConnectionsKey, info.ConnectionId),
	)
	gomock.InOrder(
		agentVersions.EXPECT().
			SetEX(gomock.Any(), ignoredAgentVersionKey, info.AgentMeta.Version, gomock.Any(), gomock.Any()),
	)
	gomock.InOrder(
		connectionsByAgentVersion.EXPECT().
			SetEX(gomock.Any(), info.AgentMeta.Version, info.ConnectionId, gomock.Any(), gomock.Any()),
		connectionsByAgentVersion.EXPECT().
			Unset(gomock.Any(), info.AgentMeta.Version, info.ConnectionId),
	)
	go func() {
		assert.NoError(t, r.RegisterConnection(context.Background(), info))
		assert.NoError(t, r.UnregisterConnection(context.Background(), info))
		cancel()
	}()

	require.NoError(t, r.Run(ctx))
}

func TestUnregisterConnection_AllCalledOnError(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	r, connectedAgents, connections, byAgentID, byProjectID, agentVersions, connectionsByAgentVersion, _, info := setupTracker(t)

	err1 := errors.New("err1")
	err2 := errors.New("err2")
	err3 := errors.New("err3")

	gomock.InOrder(
		byProjectID.EXPECT().
			Set(gomock.Any(), info.ProjectId, info.ConnectionId, gomock.Any()),
		byProjectID.EXPECT().
			Unset(gomock.Any(), info.ProjectId, info.ConnectionId).
			Return(err1),
	)
	gomock.InOrder(
		byAgentID.EXPECT().
			Set(gomock.Any(), info.AgentId, info.ConnectionId, gomock.Any()),
		byAgentID.EXPECT().
			Unset(gomock.Any(), info.AgentId, info.ConnectionId).
			Return(err2),
	)
	gomock.InOrder(
		connectedAgents.EXPECT().
			Set(gomock.Any(), ignoredConnectedAgentsKey, info.AgentId, gomock.Any()),
		connectedAgents.EXPECT().
			Forget(ignoredConnectedAgentsKey, info.AgentId),
	)
	gomock.InOrder(
		connections.EXPECT().
			Set(gomock.Any(), ignoredConnectionsKey, info.ConnectionId, gomock.Any()),
		connections.EXPECT().
			Forget(ignoredConnectionsKey, info.ConnectionId),
	)
	gomock.InOrder(
		agentVersions.EXPECT().
			SetEX(gomock.Any(), ignoredAgentVersionKey, info.AgentMeta.Version, gomock.Any(), gomock.Any()),
	)
	gomock.InOrder(
		connectionsByAgentVersion.EXPECT().
			SetEX(gomock.Any(), info.AgentMeta.Version, info.ConnectionId, gomock.Any(), gomock.Any()),
		connectionsByAgentVersion.EXPECT().
			Unset(gomock.Any(), info.AgentMeta.Version, info.ConnectionId).
			Return(err3),
	)

	go func() {
		assert.NoError(t, r.RegisterConnection(context.Background(), info))
		err := r.UnregisterConnection(context.Background(), info)
		assert.True(t, containsError(err, err1, err2, err3))
		cancel()
	}()

	require.NoError(t, r.Run(ctx))
}

func TestGC_HappyPath(t *testing.T) {
	r, connectedAgents, connections, byAgentID, byProjectID, agentVersions, connectionsByAgentVersion, _, _ := setupTracker(t)

	wasCalled1 := false
	wasCalled2 := false
	wasCalled3 := false
	wasCalled4 := false
	wasCalled5 := false
	wasCalled6 := false
	expectedKeysDeleted := 21 // sum(1..6)

	connectedAgents.EXPECT().
		GC().
		Return(func(_ context.Context) (int, error) {
			wasCalled3 = true
			return 3, nil
		})

	connections.EXPECT().
		GC().
		Return(func(_ context.Context) (int, error) {
			wasCalled6 = true
			return 6, nil
		})

	byAgentID.EXPECT().
		GC().
		Return(func(_ context.Context) (int, error) {
			wasCalled2 = true
			return 2, nil
		})

	byProjectID.EXPECT().
		GC().
		Return(func(_ context.Context) (int, error) {
			wasCalled1 = true
			return 1, nil
		})

	agentVersions.EXPECT().
		ScanAndGC(gomock.Any(), ignoredAgentVersionKey, gomock.Any()).
		DoAndReturn(func(ctx context.Context, key int64, cb redistool.ScanAndGCCallback) (int, error) {
			wasCalled4 = true
			_, err := cb("v16.9.0", nil, nil)
			return 4, err
		})

	connectionsByAgentVersion.EXPECT().
		GCFor([]string{"v16.9.0"}).
		Return(func(_ context.Context) (int, error) {
			wasCalled5 = true
			return 5, nil
		})

	assert.EqualValues(t, expectedKeysDeleted, r.runGC(context.Background()))
	assert.True(t, wasCalled1)
	assert.True(t, wasCalled2)
	assert.True(t, wasCalled3)
	assert.True(t, wasCalled4)
	assert.True(t, wasCalled5)
	assert.True(t, wasCalled6)
}

func TestGC_AllCalledOnError(t *testing.T) {
	r, connectedAgents, connections, byAgentID, byProjectID, agentVersions, connectionsByAgentVersion, rep, _ := setupTracker(t)

	wasCalled1 := false
	wasCalled2 := false
	wasCalled3 := false
	wasCalled4 := false
	wasCalled5 := false
	wasCalled6 := false
	expectedKeysDeleted := 21 // sum(1..6)

	gomock.InOrder(
		connectedAgents.EXPECT().
			GC().
			Return(func(_ context.Context) (int, error) {
				wasCalled3 = true
				return 3, errors.New("err3")
			}),
		rep.EXPECT().
			HandleProcessingError(gomock.Any(), gomock.Any(), "Failed to GC data in Redis", matcher.ErrorEq("err3")),
	)

	gomock.InOrder(
		connections.EXPECT().
			GC().
			Return(func(_ context.Context) (int, error) {
				wasCalled6 = true
				return 6, errors.New("err6")
			}),
		rep.EXPECT().
			HandleProcessingError(gomock.Any(), gomock.Any(), "Failed to GC data in Redis", matcher.ErrorEq("err6")),
	)

	gomock.InOrder(
		byAgentID.EXPECT().
			GC().
			Return(func(_ context.Context) (int, error) {
				wasCalled2 = true
				return 2, errors.New("err2")
			}),
		rep.EXPECT().
			HandleProcessingError(gomock.Any(), gomock.Any(), "Failed to GC data in Redis", matcher.ErrorEq("err2")),
	)

	gomock.InOrder(
		byProjectID.EXPECT().
			GC().
			Return(func(_ context.Context) (int, error) {
				wasCalled1 = true
				return 1, errors.New("err1")
			}),
		rep.EXPECT().
			HandleProcessingError(gomock.Any(), gomock.Any(), "Failed to GC data in Redis", matcher.ErrorEq("err1")),
	)

	err4 := errors.New("err4")
	gomock.InOrder(
		agentVersions.EXPECT().
			ScanAndGC(gomock.Any(), ignoredAgentVersionKey, gomock.Any()).
			DoAndReturn(func(ctx context.Context, key int64, cb redistool.ScanAndGCCallback) (int, error) {
				wasCalled4 = true
				_, _ = cb("v16.9.0", nil, nil)
				return 4, err4
			}),
	)

	err5 := errors.New("err5")
	gomock.InOrder(
		connectionsByAgentVersion.EXPECT().
			GCFor([]string{"v16.9.0"}).
			Return(func(_ context.Context) (int, error) {
				wasCalled5 = true
				return 5, err5
			}),

		rep.EXPECT().
			HandleProcessingError(gomock.Any(), gomock.Any(), "Failed to GC data in Redis", errors.Join(err4, err5)),
	)

	assert.EqualValues(t, expectedKeysDeleted, r.runGC(context.Background()))
	assert.True(t, wasCalled1)
	assert.True(t, wasCalled2)
	assert.True(t, wasCalled3)
	assert.True(t, wasCalled4)
	assert.True(t, wasCalled5)
	assert.True(t, wasCalled6)
}

func TestRefresh_HappyPath(t *testing.T) {
	r, connectedAgents, connections, byAgentID, byProjectID, _, _, _, _ := setupTracker(t)

	connectedAgents.EXPECT().
		Refresh(gomock.Any(), gomock.Any())
	connections.EXPECT().
		Refresh(gomock.Any(), gomock.Any())
	byAgentID.EXPECT().
		Refresh(gomock.Any(), gomock.Any())
	byProjectID.EXPECT().
		Refresh(gomock.Any(), gomock.Any())
	r.refreshRegistrations(context.Background(), time.Now())
}

func TestRefresh_AllCalledOnError(t *testing.T) {
	r, connectedAgents, connections, byAgentID, byProjectID, _, _, rep, _ := setupTracker(t)

	gomock.InOrder(
		connectedAgents.EXPECT().
			Refresh(gomock.Any(), gomock.Any()).
			Return(errors.New("err3")),
		rep.EXPECT().
			HandleProcessingError(gomock.Any(), gomock.Any(), "Failed to refresh hash data in Redis", matcher.ErrorEq("err3")),
	)
	gomock.InOrder(
		connections.EXPECT().
			Refresh(gomock.Any(), gomock.Any()).
			Return(errors.New("err4")),
		rep.EXPECT().
			HandleProcessingError(gomock.Any(), gomock.Any(), "Failed to refresh hash data in Redis", matcher.ErrorEq("err4")),
	)
	gomock.InOrder(
		byAgentID.EXPECT().
			Refresh(gomock.Any(), gomock.Any()).
			Return(errors.New("err1")),
		rep.EXPECT().
			HandleProcessingError(gomock.Any(), gomock.Any(), "Failed to refresh hash data in Redis", matcher.ErrorEq("err1")),
	)
	gomock.InOrder(
		byProjectID.EXPECT().
			Refresh(gomock.Any(), gomock.Any()).
			Return(errors.New("err2")),
		rep.EXPECT().
			HandleProcessingError(gomock.Any(), gomock.Any(), "Failed to refresh hash data in Redis", matcher.ErrorEq("err2")),
	)

	r.refreshRegistrations(context.Background(), time.Now())
}

func TestGetConnectionsByProjectID_HappyPath(t *testing.T) {
	r, _, _, _, byProjectID, _, _, _, info := setupTracker(t)
	infoBytes, err := proto.Marshal(info)
	require.NoError(t, err)
	byProjectID.EXPECT().
		ScanAndGC(gomock.Any(), info.ProjectId, gomock.Any()).
		Do(func(ctx context.Context, key int64, cb redistool.ScanAndGCCallback) (int, error) {
			var done bool
			done, err = cb("k2", infoBytes, nil)
			if err != nil || done {
				return 0, err
			}
			return 0, nil
		})
	var cbCalled int
	err = r.GetConnectionsByProjectID(context.Background(), info.ProjectId, func(i *ConnectedAgentInfo) (done bool, err error) {
		cbCalled++
		assert.Empty(t, cmp.Diff(i, info, protocmp.Transform()))
		return false, nil
	})
	require.NoError(t, err)
	assert.EqualValues(t, 1, cbCalled)
}

func TestGetConnectionsByProjectID_ScanError(t *testing.T) {
	r, _, _, _, byProjectID, _, _, rep, info := setupTracker(t)
	gomock.InOrder(
		byProjectID.EXPECT().
			ScanAndGC(gomock.Any(), info.ProjectId, gomock.Any()).
			Do(func(ctx context.Context, key int64, cb redistool.ScanAndGCCallback) (int, error) {
				done, err := cb("", nil, errors.New("intended error"))
				require.NoError(t, err)
				assert.False(t, done)
				return 0, nil
			}),
		rep.EXPECT().
			HandleProcessingError(gomock.Any(), gomock.Any(), "Redis hash scan", matcher.ErrorEq("intended error")),
	)
	err := r.GetConnectionsByProjectID(context.Background(), info.ProjectId, func(i *ConnectedAgentInfo) (done bool, err error) {
		require.FailNow(t, "unexpected call")
		return false, nil
	})
	require.NoError(t, err)
}

func TestGetConnectionsByProjectID_UnmarshalError(t *testing.T) {
	r, _, _, _, byProjectID, _, _, rep, info := setupTracker(t)
	gomock.InOrder(
		byProjectID.EXPECT().
			ScanAndGC(gomock.Any(), info.ProjectId, gomock.Any()).
			Do(func(ctx context.Context, key int64, cb redistool.ScanAndGCCallback) (int, error) {
				done, err := cb("k2", []byte{1, 2, 3}, nil) // invalid bytes
				require.NoError(t, err)                     // ignores error to keep going
				assert.False(t, done)
				return 0, nil
			}),
		rep.EXPECT().
			HandleProcessingError(gomock.Any(), gomock.Any(), "Redis proto.Unmarshal(ConnectedAgentInfo)", matcher.ErrorIs(proto.Error)),
	)
	err := r.GetConnectionsByProjectID(context.Background(), info.ProjectId, func(i *ConnectedAgentInfo) (done bool, err error) {
		require.FailNow(t, "unexpected call")
		return false, nil
	})
	require.NoError(t, err)
}

func TestGetConnectionsByAgentID_HappyPath(t *testing.T) {
	r, _, _, byAgentID, _, _, _, _, info := setupTracker(t)
	infoBytes, err := proto.Marshal(info)
	require.NoError(t, err)
	byAgentID.EXPECT().
		ScanAndGC(gomock.Any(), info.AgentId, gomock.Any()).
		Do(func(ctx context.Context, key int64, cb redistool.ScanAndGCCallback) (int, error) {
			var done bool
			done, err = cb("k2", infoBytes, nil)
			if err != nil || done {
				return 0, err
			}
			return 0, nil
		})
	var cbCalled int
	err = r.GetConnectionsByAgentID(context.Background(), info.AgentId, func(i *ConnectedAgentInfo) (done bool, err error) {
		cbCalled++
		assert.Empty(t, cmp.Diff(i, info, protocmp.Transform()))
		return false, nil
	})
	require.NoError(t, err)
	assert.EqualValues(t, 1, cbCalled)
}

func TestGetConnectionsByAgentID_ScanError(t *testing.T) {
	r, _, _, byAgentID, _, _, _, rep, info := setupTracker(t)
	gomock.InOrder(
		byAgentID.EXPECT().
			ScanAndGC(gomock.Any(), info.AgentId, gomock.Any()).
			Do(func(ctx context.Context, key int64, cb redistool.ScanAndGCCallback) (int, error) {
				done, err := cb("", nil, errors.New("intended error"))
				require.NoError(t, err)
				assert.False(t, done)
				return 0, nil
			}),
		rep.EXPECT().
			HandleProcessingError(gomock.Any(), gomock.Any(), "Redis hash scan", matcher.ErrorEq("intended error")),
	)
	err := r.GetConnectionsByAgentID(context.Background(), info.AgentId, func(i *ConnectedAgentInfo) (done bool, err error) {
		require.FailNow(t, "unexpected call")
		return false, nil
	})
	require.NoError(t, err)
}

func TestGetConnectionsByAgentID_UnmarshalError(t *testing.T) {
	r, _, _, byAgentID, _, _, _, rep, info := setupTracker(t)
	byAgentID.EXPECT().
		ScanAndGC(gomock.Any(), info.AgentId, gomock.Any()).
		Do(func(ctx context.Context, key int64, cb redistool.ScanAndGCCallback) (int, error) {
			done, err := cb("k2", []byte{1, 2, 3}, nil) // invalid bytes
			require.NoError(t, err)                     // ignores error to keep going
			assert.False(t, done)
			return 0, nil
		})
	rep.EXPECT().
		HandleProcessingError(gomock.Any(), gomock.Any(), "Redis proto.Unmarshal(ConnectedAgentInfo)", matcher.ErrorIs(proto.Error))
	err := r.GetConnectionsByAgentID(context.Background(), info.AgentId, func(i *ConnectedAgentInfo) (done bool, err error) {
		require.FailNow(t, "unexpected call")
		return false, nil
	})
	require.NoError(t, err)
}

func TestGetConnectedAgentsCount_HappyPath(t *testing.T) {
	r, connectedAgents, _, _, _, _, _, _, _ := setupTracker(t)
	connectedAgents.EXPECT().
		Len(gomock.Any(), ignoredConnectedAgentsKey).
		Return(int64(1), nil)
	size, err := r.GetConnectedAgentsCount(context.Background())
	require.NoError(t, err)
	assert.EqualValues(t, 1, size)
}

func TestGetConnectedAgentsCount_LenError(t *testing.T) {
	r, connectedAgents, _, _, _, _, _, _, _ := setupTracker(t)
	connectedAgents.EXPECT().
		Len(gomock.Any(), ignoredConnectedAgentsKey).
		Return(int64(0), errors.New("intended error"))
	size, err := r.GetConnectedAgentsCount(context.Background())
	require.Error(t, err)
	assert.Zero(t, size)
}

func TestGetConnectionsCount_HappyPath(t *testing.T) {
	r, _, connections, _, _, _, _, _, _ := setupTracker(t)
	connections.EXPECT().
		Len(gomock.Any(), ignoredConnectionsKey).
		Return(int64(1), nil)
	size, err := r.GetConnectionsCount(context.Background())
	require.NoError(t, err)
	assert.EqualValues(t, 1, size)
}

func TestGetConnectionCount_LenError(t *testing.T) {
	r, _, connections, _, _, _, _, _, _ := setupTracker(t)
	connections.EXPECT().
		Len(gomock.Any(), ignoredConnectionsKey).
		Return(int64(0), errors.New("intended error"))
	size, err := r.GetConnectionsCount(context.Background())
	require.Error(t, err)
	assert.Zero(t, size)
}

func TestCountAgentsByAgentVersions_HappyPath(t *testing.T) {
	r, _, _, _, _, agentVersions, connectionsByAgentVersion, _, _ := setupTracker(t)
	agentVersions.EXPECT().
		ScanAndGC(gomock.Any(), ignoredAgentVersionKey, gomock.Any()).
		DoAndReturn(func(ctx context.Context, key int64, cb redistool.ScanAndGCCallback) (int, error) {
			_, err := cb("16.8.0", nil, nil)
			require.NoError(t, err)
			_, err = cb("16.9.0", nil, nil)
			require.NoError(t, err)
			return 1, nil
		})
	connectionsByAgentVersion.EXPECT().
		Len(gomock.Any(), "16.8.0").
		Return(int64(111), nil)
	connectionsByAgentVersion.EXPECT().
		Len(gomock.Any(), "16.9.0").
		Return(int64(222), nil)
	counts, err := r.CountAgentsByAgentVersions(context.Background())
	require.NoError(t, err)
	assert.Len(t, counts, 2)
	assert.EqualValues(t, 111, counts["16.8.0"])
	assert.EqualValues(t, 222, counts["16.9.0"])
}

func TestCountAgentsByAgentVersions_getAgentVersionsError(t *testing.T) {
	r, _, _, _, _, agentVersions, _, _, _ := setupTracker(t)
	agentVersions.EXPECT().
		ScanAndGC(gomock.Any(), ignoredAgentVersionKey, gomock.Any()).
		DoAndReturn(func(ctx context.Context, key int64, cb redistool.ScanAndGCCallback) (int, error) {
			return 0, errors.New("intended error")
		})
	counts, err := r.CountAgentsByAgentVersions(context.Background())
	assert.EqualError(t, err, "failed to get agent versions from Redis: intended error")
	assert.Nil(t, counts)
}

func TestCountAgentsByAgentVersions_LenError(t *testing.T) {
	r, _, _, _, _, agentVersions, connectionsByAgentVersion, _, _ := setupTracker(t)
	agentVersions.EXPECT().
		ScanAndGC(gomock.Any(), ignoredAgentVersionKey, gomock.Any()).
		DoAndReturn(func(ctx context.Context, key int64, cb redistool.ScanAndGCCallback) (int, error) {
			_, err := cb("16.8.0", nil, nil)
			require.NoError(t, err)
			_, err = cb("16.9.0", nil, nil)
			require.NoError(t, err)
			return 1, nil
		})
	connectionsByAgentVersion.EXPECT().
		Len(gomock.Any(), "16.8.0").
		Return(int64(0), errors.New("intended error"))
	counts, err := r.CountAgentsByAgentVersions(context.Background())
	assert.EqualError(t, err, "failed to get hash length from connectionsByAgentVersion in Redis: intended error")
	assert.Nil(t, counts)
}

func setupTracker(t *testing.T) (*RedisTracker, *mock_redis.MockExpiringHash[int64, int64],
	*mock_redis.MockExpiringHash[int64, int64], *mock_redis.MockExpiringHash[int64, int64],
	*mock_redis.MockExpiringHash[int64, int64], *mock_redis.MockExpiringHash[int64, string],
	*mock_redis.MockExpiringHash[string, int64], *mock_tool.MockErrReporter, *ConnectedAgentInfo) {
	ctrl := gomock.NewController(t)
	rep := mock_tool.NewMockErrReporter(ctrl)
	connectedAgents := mock_redis.NewMockExpiringHash[int64, int64](ctrl)
	connections := mock_redis.NewMockExpiringHash[int64, int64](ctrl)
	byAgentID := mock_redis.NewMockExpiringHash[int64, int64](ctrl)
	byProjectID := mock_redis.NewMockExpiringHash[int64, int64](ctrl)
	agentVersions := mock_redis.NewMockExpiringHash[int64, string](ctrl)
	connectionsByAgentVersion := mock_redis.NewMockExpiringHash[string, int64](ctrl)
	tr := &RedisTracker{
		log:                       zaptest.NewLogger(t),
		errRep:                    rep,
		refreshPeriod:             time.Minute,
		gcPeriod:                  time.Minute,
		connectionsByAgentID:      byAgentID,
		connectionsByProjectID:    byProjectID,
		connectedAgents:           connectedAgents,
		connections:               connections,
		agentVersions:             agentVersions,
		connectionsByAgentVersion: connectionsByAgentVersion,
	}
	return tr, connectedAgents, connections, byAgentID, byProjectID, agentVersions, connectionsByAgentVersion, rep, connInfo()
}

func connInfo() *ConnectedAgentInfo {
	return &ConnectedAgentInfo{
		AgentMeta: &entity.AgentMeta{
			Version:      "v1.2.3",
			CommitId:     "123123",
			PodNamespace: "ns",
			PodName:      "name",
		},
		ConnectedAt:  timestamppb.Now(),
		ConnectionId: 123,
		AgentId:      345,
		ProjectId:    456,
	}
}

func containsError(err error, errs ...error) bool {
	for _, e := range errs {
		if errors.Is(err, e) {
			return true
		}
	}
	return false
}
