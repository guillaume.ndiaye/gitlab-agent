load("//build:build.bzl", "go_custom_test")
load("@io_bazel_rules_go//go:def.bzl", "go_library")

go_library(
    name = "kasapp",
    srcs = [
        "agent_rpc_api.go",
        "app.go",
        "app_agent_server.go",
        "app_api_server.go",
        "app_internal_server.go",
        "app_private_api_server.go",
        "configured_app.go",
        "defaulting.go",
        "doc.go",
        "rpc_api.go",
        "server_api.go",
    ],
    importpath = "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/cmd/kas/kasapp",
    visibility = ["//visibility:public"],
    deps = [
        "//cmd",
        "//internal/api",
        "//internal/gitaly",
        "//internal/gitaly/vendored/client",
        "//internal/gitlab",
        "//internal/gitlab/api",
        "//internal/module/agent_configuration/server",
        "//internal/module/agent_registrar",
        "//internal/module/agent_registrar/server",
        "//internal/module/agent_tracker",
        "//internal/module/agent_tracker/server",
        "//internal/module/configuration_project/server",
        "//internal/module/event_tracker",
        "//internal/module/event_tracker/server",
        "//internal/module/flux/server",
        "//internal/module/gitlab_access/server",
        "//internal/module/gitops/server",
        "//internal/module/google_profiler/server",
        "//internal/module/kubernetes_api/server",
        "//internal/module/modserver",
        "//internal/module/modshared",
        "//internal/module/notifications/server",
        "//internal/module/observability",
        "//internal/module/observability/server",
        "//internal/module/reverse_tunnel/server",
        "//internal/module/usage_metrics",
        "//internal/module/usage_metrics/server",
        "//internal/tool/cache",
        "//internal/tool/errz",
        "//internal/tool/grpctool",
        "//internal/tool/httpz",
        "//internal/tool/ioz",
        "//internal/tool/logz",
        "//internal/tool/metric",
        "//internal/tool/nettool",
        "//internal/tool/prototool",
        "//internal/tool/redistool",
        "//internal/tool/retry",
        "//internal/tool/syncz",
        "//internal/tool/tlstool",
        "//internal/tool/wstunnel",
        "//internal/tunnel/kas",
        "//internal/tunnel/tunserver",
        "//pkg/event",
        "//pkg/kascfg",
        "@com_github_ash2k_stager//:stager",
        "@com_github_getsentry_sentry_go//:sentry-go",
        "@com_github_go_logr_zapr//:zapr",
        "@com_github_grpc_ecosystem_go_grpc_middleware_providers_prometheus//:prometheus",
        "@com_github_grpc_ecosystem_go_grpc_middleware_v2//interceptors/auth",
        "@com_github_grpc_ecosystem_go_grpc_middleware_v2//interceptors/validator",
        "@com_github_prometheus_client_golang//prometheus",
        "@com_github_prometheus_client_golang//prometheus/collectors",
        "@com_github_redis_rueidis//:rueidis",
        "@com_github_redis_rueidis_rueidisotel//:rueidisotel",
        "@com_github_spf13_cobra//:cobra",
        "@io_k8s_klog_v2//:klog",
        "@io_k8s_sigs_yaml//:yaml",
        "@io_nhooyr_websocket//:websocket",
        "@io_opentelemetry_go_contrib_instrumentation_google_golang_org_grpc_otelgrpc//:otelgrpc",
        "@io_opentelemetry_go_contrib_instrumentation_net_http_otelhttp//:otelhttp",
        "@io_opentelemetry_go_otel//:otel",
        "@io_opentelemetry_go_otel//attribute",
        "@io_opentelemetry_go_otel//propagation",
        "@io_opentelemetry_go_otel//semconv/v1.24.0:v1_24_0",
        "@io_opentelemetry_go_otel_exporters_otlp_otlptrace_otlptracehttp//:otlptracehttp",
        "@io_opentelemetry_go_otel_exporters_prometheus//:prometheus",
        "@io_opentelemetry_go_otel_metric//:metric",
        "@io_opentelemetry_go_otel_sdk//resource",
        "@io_opentelemetry_go_otel_sdk//trace",
        "@io_opentelemetry_go_otel_sdk_metric//:metric",
        "@io_opentelemetry_go_otel_trace//:trace",
        "@io_opentelemetry_go_otel_trace//noop",
        "@org_golang_google_grpc//:grpc",
        "@org_golang_google_grpc//backoff",
        "@org_golang_google_grpc//codes",
        "@org_golang_google_grpc//credentials",
        "@org_golang_google_grpc//credentials/insecure",
        "@org_golang_google_grpc//encoding/gzip",
        "@org_golang_google_grpc//grpclog",
        "@org_golang_google_grpc//keepalive",
        "@org_golang_google_grpc//stats",
        "@org_golang_google_grpc//status",
        "@org_golang_google_protobuf//encoding/protojson",
        "@org_golang_google_protobuf//proto",
        "@org_golang_google_protobuf//types/known/anypb",
        "@org_golang_x_time//rate",
        "@org_uber_go_zap//:zap",
        "@org_uber_go_zap//zapcore",
        "@org_uber_go_zap//zapgrpc",
    ],
)

go_custom_test(
    name = "kasapp_test",
    srcs = [
        "agent_rpc_api_test.go",
        "app_private_api_server_test.go",
        "app_test.go",
        "configured_app_test.go",
        "mock_for_test.go",
        "server_api_test.go",
    ],
    embed = [":kasapp"],
    deps = [
        "//internal/api",
        "//internal/gitlab/api",
        "//internal/module/modserver",
        "//internal/module/modshared",
        "//internal/tool/cache",
        "//internal/tool/errz",
        "//internal/tool/prototool",
        "//internal/tool/testing/mock_cache",
        "//internal/tool/testing/mock_gitlab",
        "//internal/tool/testing/testhelpers",
        "//internal/tunnel/rpc",
        "@com_github_getsentry_sentry_go//:sentry-go",
        "@com_github_google_go_cmp//cmp",
        "@com_github_stretchr_testify//assert",
        "@com_github_stretchr_testify//require",
        "@io_opentelemetry_go_otel_trace//:trace",
        "@io_opentelemetry_go_otel_trace//noop",
        "@org_golang_google_grpc//:grpc",
        "@org_golang_google_grpc//codes",
        "@org_golang_google_grpc//status",
        "@org_golang_google_protobuf//proto",
        "@org_golang_google_protobuf//testing/protocmp",
        "@org_uber_go_mock//gomock",
        "@org_uber_go_zap//:zap",
        "@org_uber_go_zap//zaptest",
    ],
)
